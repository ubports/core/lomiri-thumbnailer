#!/bin/sh -euf
#
# Copyright (C) 2021 UBports Foundation.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License version 3 as
# published by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# Authored by: Ratchanan Srirattanamet <ratchanan@ubports.com>

# Note: can afford to do destructive migration since this is just a cache.

old_path="${HOME}/.cache/unity-thumbnailer"
new_path="${HOME}/.cache/lomiri-thumbnailer"

if [ -d "$old_path" ]; then
    if ! [ -d "$new_path" ]; then
        mv "$old_path" "$new_path"
    else
        rm -r "$old_path"
    fi
fi
