Source: lomiri-thumbnailer
Section: libdevel
Priority: optional
Maintainer: UBports Developers <developers@ubports.com>
Build-Depends: cmake,
               cmake-extras (>= 0.4),
               debhelper-compat (= 13),
               dh-migrations | hello,
               google-mock,
               gstreamer1.0-libav,
               gstreamer1.0-plugins-bad-faad,
               gstreamer1.0-plugins-good,
               libapparmor-dev,
               libboost-dev,
               libboost-filesystem-dev,
               libboost-iostreams-dev,
               libboost-regex-dev,
               libexif-dev,
               libgdk-pixbuf-2.0-dev,
               libgstreamer1.0-dev,
               libgstreamer-plugins-base1.0-dev,
               libgtest-dev,
               libleveldb-dev,
               libqtdbustest1-dev,
               librsvg2-common,
               libtag1-dev,
               liblomiri-api-dev,
               licensecheck,
               lsb-release,
               persistent-cache-cpp-dev (>= 1.0.7-1~),
               python3-tornado <!nocheck>,
               qml-module-qtquick2,
               qml-module-qttest,
               qtbase5-dev,
               qtbase5-dev-tools,
               qtdeclarative5-dev,
               shared-mime-info,
               xauth,
               xvfb,
               doxygen,
Standards-Version: 4.6.2
Rules-Requires-Root: no
Homepage: https://gitlab.com/ubports/core/lomiri-thumbnailer

Package: lomiri-thumbnailer-service
Architecture: any
Multi-Arch: foreign
Pre-Depends: ${misc:Pre-Depends},
Depends: dconf-cli,
         ${misc:Depends},
         ${shlibs:Depends},
Conflicts: libthumbnailer0, thumbnailer-common
Replaces: libthumbnailer0, thumbnailer-common
Description: D-Bus service for out of process thumbnailing
 Lomiri Thumbnailer is an API for the Lomiri operating environment to
 create and cache image thumbnails for local and remote media.
 .
 This package provides thumbnailer D-Bus service that can provide
 thumbnails on behalf of another process.

Package: qml-module-lomiri-thumbnailer
Section: libdevel
Architecture: any
Multi-Arch: same
Pre-Depends: ${misc:Pre-Depends},
Depends: ${misc:Depends},
         ${shlibs:Depends},
         liblomiri-thumbnailer-qt1.0 (= ${binary:Version}),
Recommends: lomiri-thumbnailer-service (= ${binary:Version}),
Breaks: qml-module-lomiri-thumbnailer0.1,
Replaces: qml-module-lomiri-thumbnailer0.1,
Provides: qml-module-lomiri-thumbnailer0.1,
Description: QML interface for the thumbnailer
 Lomiri Thumbnailer is an API for the Lomiri operating environment to
 create and cache image thumbnails for local and remote media.
 .
 This package provides image providers that allow access to the
 thumbnailer from Qt Quick 2 / QML applications.

Package: qml-module-ubuntu-thumbnailer0.1
Architecture: any
Multi-Arch: same
Pre-Depends: ${misc:Pre-Depends},
Depends: ${misc:Depends},
         ${shlibs:Depends},
         qml-module-lomiri-thumbnailer0.1 (= ${binary:Version}),
Provides: ubuntu-thumbnailer-impl,
          ubuntu-thumbnailer-impl-0,
Breaks: qtdeclarative5-ubuntu-thumbnailer0.1 (<< 2.3~),
        qml-module-ubuntu-thumbnailer0.1 (<< 3.0~),
Replaces: qtdeclarative5-ubuntu-thumbnailer0.1 (<< 2.3~),
          qml-module-ubuntu-thumbnailer0.1 (<< 3.0~)
Description: QML interface for the thumbnailer (Ubuntu compatibility layer).
 Lomiri Thumbnailer is an API for the Lomiri operating environment to
 create and cache image thumbnails for local and remote media.
 .
 This package provides a compatibility layer for the Ubuntu.Thumbnailer QML
 type. Other distros other than UBports' Ubuntu Touch likely doesn't care
 about this package.

Package: liblomiri-thumbnailer-qt1.0
Section: libs
Architecture: any
Multi-Arch: same
Pre-Depends: ${misc:Pre-Depends},
Depends: ${misc:Depends},
         ${shlibs:Depends},
         lomiri-thumbnailer-service,
Description: Qt/C++ API to obtain thumbnails
 Lomiri Thumbnailer is an API for the Lomiri operating environment to
 create and cache image thumbnails for local and remote media.
 .
 Library providing a client API for obtaining thumbnails from the DBus
 service.

Package: liblomiri-thumbnailer-qt-dev
Section: libdevel
Architecture: any
Multi-Arch: same
Pre-Depends: ${misc:Pre-Depends},
Depends: liblomiri-thumbnailer-qt1.0 (= ${binary:Version}),
         ${misc:Depends},
Description: Qt/C++ API to obtain thumbnails (development files)
 Lomiri Thumbnailer is an API for the Lomiri operating environment to
 create and cache image thumbnails for local and remote media.
 .
 Library providing a client API for obtaining thumbnails from the DBus
 service.
 .
 This package provides the development files for the library.

Package: liblomiri-thumbnailer-qt-doc
Section: doc
Architecture: all
Multi-Arch: foreign
Depends: ${misc:Depends},
Description: Documentation for Thumbnailer API
 Lomiri Thumbnailer is an API for the Lomiri operating environment to
 create and cache image thumbnails for local and remote media.
 .
 API documentation of Thumbnailer API.

