add_executable(dbus_test dbus_test.cpp)
target_link_libraries(dbus_test
    thumbnailer-static
    testutils
    Qt5::Network
    Qt5::DBus
    Qt5::Test
    gtest
)
add_test(dbus dbus_test)
add_dependencies(dbus_test thumbnailer-service)
